﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace DIContainer
{

    class Employee
    {
        public string Name { get; set; }
        IDBAccess _DBAccess;

        public Employee(IDBAccess DBAccess)
        {
            _DBAccess = DBAccess;
        }

    }

    class SQLDataAccess : IDBAccess
    {

        public string _connection;
        public string connection
        {
            get
            {
                return "test connection";
            }
            set
            {
                _connection = value;
            }
        }
    }


    interface IDBAccess
    {
        string connection { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.RegisterType<IDBAccess, SQLDataAccess>();
            Employee emp = container.Resolve<Employee>();
        }
    }
}
